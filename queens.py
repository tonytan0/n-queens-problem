from pprint import pprint

def is_safe(board, row_index, column_index, size):
    # Check the row for other queens
    row = board[row_index]
    for index, value in enumerate(row):
        if value == 1:
            return False
    
    # Check the diagonals
    for index in range(1, size - column_index + 1):
        if row_index - index >= 0 and column_index + index < size:
            if board[row_index - index][column_index + index] != 0:
                return False
        if row_index + index < size and column_index + index < size:
            if board[row_index + index][column_index + index] != 0:
                return False

    for index in range (column_index - 1, -1, -1):
        if row_index - index >= 0 and column_index - index >= 0:
            if board[row_index - index][column_index - index] != 0:
                return False
        if row_index + index < size and column_index - index >= 0:
            if board[row_index + index][column_index - index] != 0:
                return False
    return True

def place_queen_in_column(board, column_index, size):
    if column_index >= size:
        return True
    for row_index in range(size):
        if is_safe(board, row_index, column_index, size):
            board[row_index][column_index] = 1
            if place_queen_in_column(board, column_index + 1, size):
                return True
            board[row_index][column_index] = 0
    return False

# Once we have that, we can write the function to place the queen in the specific column.
#   It checks to see if we're at the edge of the board and returns True, if so
#   It then tries each row from 0 to size
#   If the move is safe, put the queen on the board
#       If placing it in the next column is good, then return True
#       Otherwise, remove the queen from the board and do the loop
# Return False if we couldn't find a place to put it



def n_queens(size):
    board = []
    for i in range(size):
        row = [0] * size
        board.append(row)

    place_queen_in_column(board, 0, size)

    return board
# Start with left most column of chess board
# For i = 0 to n
#   If we're past the edge of the board, we found a solution, so return True
#   Place the queen in the ith row
#   If the move is safe
#       Move to the next column
#       Jump back to step 2
#       If we found a solution, then return True
#       Remove the queen from the ith row
#   Return False because we did not find a solution
# Return the board with the solution
