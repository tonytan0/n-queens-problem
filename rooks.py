from pprint import pprint


def is_in_danger(board, row_index, column_index):
    #check the row for a rook
    for index,value in enumerate(board [row_index]):
        if value == 0 or index == column_index:
            pass
        else:
            return True

    #check the col for a rook
    # for index,row in enumerate (board):
    #     if  row[column_index] == 0 or row_index:
    #         pass
    #     else:
    #         return True
    #after checking row and col return false ... chuckles not in danger
    return False

def place_rook_in_column(board, column_index):
    #base case
    if column_index >= 4:
        return
    for row_number in range(4):
        board[row_number][column_index]=1
        
        #print(is_in_danger(board, row_number, column_index))
        if is_in_danger(board, row_number, column_index):
            #if in danger remove rook
            board[row_number][column_index] = 0
        else:
            place_rook_in_column(board, column_index + 1)





def four_rooks():
    board = [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ]

    place_rook_in_column(board, 0)
    
    return board



if __name__ == "__main__":
    board = four_rooks()
    pprint(board, width=20)